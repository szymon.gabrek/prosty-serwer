const chalk = require('chalk');
const express = require('express');
const app = express();

app.set('view engine', 'pug');

app.get('/:imie', function (req, res) {
  res.render('index', { imie: req.params.imie })
})

app.get('/strona', function (req, res) {
  res.sendFile(__dirname + '/public/index.html')
})

app.get('/potega/:liczba/:potega', function (req, res) {
  console.log( req.params );
  const liczba = req.params.liczba;
  const wykladnik = req.params.potega;
  const wynik = liczba**wykladnik;
  res.render( 'potega', { liczba: liczba, wykladnik: wykladnik, wynik: wynik });
})

app.listen(3000, function(err){
  if(err){
    console.log(chalk.red('Error: ' + err ));
    return err;
  }
  console.log( chalk.green('Server is up and running on port 3000'))
} )
